#include <systemc.h>
#include "adder.hpp"

using namespace std;

void adder::sum_perform() {
	
	sc_uint<16> sum_result;
	while(true) {
		wait();
		sum_result = adder_in1->read() + adder_in2->read();
		adder_out->write(sum_result);
 	}   

}
   
