#include <systemc.h>
#include "or_gate.hpp"

using namespace std;

void or_gate::or_gate_perform() {
	
	sc_uint<16> or_gate_in1_temp, or_gate_in2_temp;	
	sc_uint<16> or_gate_result;

	while(true) {
		
		wait();
		or_gate_in1_temp = or_gate_in1->read();
		or_gate_in2_temp = or_gate_in2->read();			
		for (unsigned i=0; i<16; i++) {
        		or_gate_result[i] = (or_gate_in1_temp[i] | or_gate_in2_temp[i]);		
		}
	
	or_gate_out->write(or_gate_result);
		
 	}   

}
   
