#include <systemc.h>
#include "add_incr.hpp"

using namespace std;

SC_HAS_PROCESS(add_incr);

add_incr::add_incr(sc_module_name name): 
	sc_module(name), 
        add1("add1"), mux2_1("mux2_1"), mux2_2("mux2_2") {

  // Port Map
  number_one = 1;
  mux2_1.mux2_in1(add_incr_in1);
  mux2_1.mux2_in2(number_one);
  mux2_1.mux2_sel(add_incr_op1);
  mux2_1.mux2_out(mux1_out);

  mux2_2.mux2_in1(number_one);
  mux2_2.mux2_in2(add_incr_in2);
  mux2_2.mux2_sel(add_incr_op2);
  mux2_2.mux2_out(mux2_out);

  add1.adder_in1(mux1_out);
  add1.adder_in2(mux2_out);
  add1.adder_out(add_incr_out);
  
  
  
}


