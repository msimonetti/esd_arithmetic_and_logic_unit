#include <systemc.h>
#include "mux16.hpp"

using namespace std;

void mux16::mux16_perform() {
	
	sc_uint<32> mux16_in1_temp, mux16_in2_temp, mux16_in3_temp, mux16_in4_temp, mux16_in5_temp, mux16_in6_temp, mux16_in7_temp, mux16_in8_temp,
                  mux16_in9_temp, mux16_in10_temp, mux16_in11_temp, mux16_in12_temp, mux16_in13_temp, mux16_in14_temp, mux16_in15_temp, mux16_in16_temp;	
	sc_uint<4> mux16_sel_temp;	
	sc_uint<32> mux16_result;

	while(true) {
		
		wait();
		mux16_in1_temp = mux16_in1->read();
		mux16_in2_temp = mux16_in2->read();			
		mux16_in3_temp = mux16_in3->read();
		mux16_in4_temp = mux16_in4->read();
		mux16_in5_temp = mux16_in5->read();
		mux16_in6_temp = mux16_in6->read();			
		mux16_in7_temp = mux16_in7->read();
		mux16_in8_temp = mux16_in8->read();			
		mux16_in9_temp = mux16_in9->read();
		mux16_in10_temp = mux16_in10->read();			
		mux16_in11_temp = mux16_in11->read();
		mux16_in12_temp = mux16_in12->read();
		mux16_in13_temp = mux16_in13->read();
		mux16_in14_temp = mux16_in14->read();			
		mux16_in15_temp = mux16_in15->read();
		mux16_in16_temp = mux16_in16->read();			
					
		mux16_sel_temp = mux16_sel->read();

		switch(mux16_sel_temp){
			case 0 : mux16_result = mux16_in1_temp; break;
			case 1 : mux16_result = mux16_in2_temp; break;
			case 2 : mux16_result = mux16_in3_temp; break;
			case 3 : mux16_result = mux16_in4_temp; break;
			case 4 : mux16_result = mux16_in5_temp; break;
			case 5 : mux16_result = mux16_in6_temp; break;
			case 6 : mux16_result = mux16_in7_temp; break;
			case 7 : mux16_result = mux16_in8_temp; break;
			case 8 : mux16_result = mux16_in9_temp; break;
			case 9 : mux16_result = mux16_in10_temp; break;
			case 10 : mux16_result = mux16_in11_temp; break;
			case 11 : mux16_result = mux16_in12_temp; break;
			case 12 : mux16_result = mux16_in13_temp; break;
			case 13 : mux16_result = mux16_in14_temp; break;
			case 14 : mux16_result = mux16_in15_temp; break;
			case 15 : mux16_result = mux16_in16_temp; break;
		
		}
		
		
		mux16_out->write(mux16_result);
		
 	}   

}
   
