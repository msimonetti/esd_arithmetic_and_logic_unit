#include <systemc.h>
#include "and_gate.hpp"

using namespace std;

void and_gate::and_gate_perform() {
	
	sc_uint<16> and_gate_in1_temp, and_gate_in2_temp;	
	sc_uint<16> and_gate_result;

	while(true) {
		
		wait();
		and_gate_in1_temp = and_gate_in1->read();
		and_gate_in2_temp = and_gate_in2->read();			
		for (unsigned i=0; i<16; i++) {
        		and_gate_result[i] = (and_gate_in1_temp[i] & and_gate_in2_temp[i]);		
		}
	
	and_gate_out->write(and_gate_result);
		
 	}   

}
   
