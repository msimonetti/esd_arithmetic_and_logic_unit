#include <systemc.h>
#include "mux2.hpp"

using namespace std;

void mux2::mux2_perform() {
	
	sc_uint<16> mux2_in1_temp, mux2_in2_temp;	
	bool mux2_sel_temp;
	sc_uint<16> mux2_result;

	while(true) {
		
		wait();
		mux2_in1_temp = mux2_in1->read();
		mux2_in2_temp = mux2_in2->read();			
			
		mux2_sel_temp = mux2_sel->read();

		switch(mux2_sel_temp){
			case 0 : mux2_result = mux2_in1_temp; break;
			case 1 : mux2_result = mux2_in2_temp; break;
			}
		
		
		mux2_out->write(mux2_result);
		
 	}   

}
   
