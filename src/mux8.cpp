#include <systemc.h>
#include "mux8.hpp"

using namespace std;

void mux8::mux8_perform() {
	
	sc_uint<16> mux8_in1_temp, mux8_in2_temp, mux8_in3_temp, mux8_in4_temp, mux8_in5_temp, mux8_in6_temp, mux8_in7_temp, mux8_in8_temp;	
	sc_uint<3> mux8_sel_temp;	
	sc_uint<16> mux8_result;

	while(true) {
		
		wait();
		mux8_in1_temp = mux8_in1->read();
		mux8_in2_temp = mux8_in2->read();			
		mux8_in3_temp = mux8_in3->read();
		mux8_in4_temp = mux8_in4->read();
		mux8_in5_temp = mux8_in5->read();
		mux8_in6_temp = mux8_in6->read();			
		mux8_in7_temp = mux8_in7->read();
		mux8_in8_temp = mux8_in8->read();			
					
		mux8_sel_temp = mux8_sel->read();

		switch(mux8_sel_temp){
			case 0 : mux8_result = mux8_in1_temp; break;
			case 1 : mux8_result = mux8_in2_temp; break;
			case 2 : mux8_result = mux8_in3_temp; break;
			case 3 : mux8_result = mux8_in4_temp; break;
			case 4 : mux8_result = mux8_in5_temp; break;
			case 5 : mux8_result = mux8_in6_temp; break;
			case 6 : mux8_result = mux8_in7_temp; break;
			case 7 : mux8_result = mux8_in8_temp; break;
		}
		
		
		mux8_out->write(mux8_result);
		
 	}   

}
   
