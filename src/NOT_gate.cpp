// NOT logic gate
#include <systemc.h>
#include "NOT_gate.hpp"

using namespace std;

void NOT_gate::notgate_operate(){
	
	sc_uint<NBIT> temp_in, temp_out;
	
	while(true){
		wait();
		temp_in = NOT_input->read();
		for(unsigned j=0; j<NBIT; j++){
			temp_out[j] = ~temp_in[j];}

		NOT_output->write(temp_out);	
	}
}
