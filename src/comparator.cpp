#include <systemc.h>
#include "comparator.hpp"

using namespace std;

void comparator::comparator_perform() {
	
	sc_uint<16> comparator_in1_temp, comparator_in2_temp;	
	sc_uint<16> comparator_result;

	while(true) {
		
		wait();
		comparator_in1_temp = comparator_in1->read();
		comparator_in2_temp = comparator_in2->read();			
	
		if(comparator_in1_temp != comparator_in2_temp)
		  	comparator_result = 0;
		else
			comparator_result = 1;



		comparator_out->write(comparator_result);
		
 	}   

}
   
