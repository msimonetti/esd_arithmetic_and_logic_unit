#ifndef reg16_HPP
#define reg16_HPP

SC_MODULE(reg16) {
  
  sc_in<sc_uint<16> > reg16_in;
  sc_in<bool>  reg16_load, reg16_clk;
  sc_out<sc_uint<16> > reg16_out;
  
  

  SC_CTOR(reg16) {
	SC_THREAD(reg16_perform);     
	sensitive << reg16_in << reg16_load << reg16_clk.pos();
     }

  
  void reg16_perform();

};

#endif
