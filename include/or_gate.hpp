#ifndef or_gate_HPP
#define or_gate_HPP

SC_MODULE(or_gate) {
  
  sc_in<sc_uint<16> > or_gate_in1;
  sc_in<sc_uint<16> > or_gate_in2;
  sc_out<sc_uint<16> > or_gate_out;
  
  

  SC_CTOR(or_gate) {
	SC_THREAD(or_gate_perform);     
	sensitive << or_gate_in1 << or_gate_in2;
     }

  
  void or_gate_perform();

};

#endif
