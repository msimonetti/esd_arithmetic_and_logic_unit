#ifndef nor_gate_HPP
#define nor_gate_HPP

SC_MODULE(nor_gate) {
  
  sc_in<sc_uint<16> > nor_gate_in1;
  sc_in<sc_uint<16> > nor_gate_in2;
  sc_out<sc_uint<16> > nor_gate_out;
  
  

  SC_CTOR(nor_gate) {
	SC_THREAD(nor_gate_perform);     
	sensitive << nor_gate_in1 << nor_gate_in2;
     }

  
  void nor_gate_perform();

};

#endif
