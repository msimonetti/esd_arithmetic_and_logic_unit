// SHIFTER

#ifndef SR_HPP
#define SR_HPP

#define NBIT 16
#define Q 4

SC_MODULE(SR){
	
	sc_in<sc_uint<NBIT> > SR_input; 
	sc_in<sc_uint<Q> > quantity; 
	sc_in<bool> 	sel;
	sc_out<sc_uint<NBIT> > SR_output;
	

	SC_CTOR(SR){
		SC_THREAD(SR_operate);
		sensitive << SR_input << sel << quantity;}

	void SR_operate();
};

#endif

