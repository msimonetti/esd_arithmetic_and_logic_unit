#ifndef mux2_HPP
#define mux2_HPP

SC_MODULE(mux2) {
  
  sc_in<sc_uint<16> > mux2_in1, mux2_in2;
  sc_in<bool> mux2_sel;
  sc_out<sc_uint<16> > mux2_out;
  
  

  SC_CTOR(mux2) {
	SC_THREAD(mux2_perform);     
	sensitive << mux2_in1 << mux2_in2 << mux2_sel;
     }

  void mux2_perform();

};

#endif
