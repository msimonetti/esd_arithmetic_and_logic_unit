#ifndef xor_gate_HPP
#define xor_gate_HPP

SC_MODULE(xor_gate) {
  
  sc_in<sc_uint<16> > xor_gate_in1;
  sc_in<sc_uint<16> > xor_gate_in2;
  sc_out<sc_uint<16> > xor_gate_out;
  
  

  SC_CTOR(xor_gate) {
	SC_THREAD(xor_gate_perform);     
	sensitive << xor_gate_in1 << xor_gate_in2;
     }

  
  void xor_gate_perform();

};

#endif
