#ifndef mux16_HPP
#define mux16_HPP

SC_MODULE(mux16) {
  
  sc_in<sc_uint<32> > mux16_in1, mux16_in2, mux16_in3, mux16_in4, mux16_in5, mux16_in6, mux16_in7, mux16_in8, 
		      mux16_in9, mux16_in10, mux16_in11, mux16_in12, mux16_in13, mux16_in14, mux16_in15, mux16_in16;
  sc_in<sc_uint<4> > mux16_sel;
  sc_out<sc_uint<32> > mux16_out;
  
  

  SC_CTOR(mux16) {
	SC_THREAD(mux16_perform);     
	sensitive << mux16_in1 << mux16_in2 << mux16_in3 << mux16_in4 << mux16_in5 << mux16_in6 << mux16_in7 << mux16_in8 << mux16_sel <<
		     mux16_in9 << mux16_in10 << mux16_in11 << mux16_in12 << mux16_in13 << mux16_in14 << mux16_in15 << mux16_in16;
     }

  void mux16_perform();

};

#endif
