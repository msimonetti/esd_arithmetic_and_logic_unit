#ifndef comparator_HPP
#define comparator_HPP

SC_MODULE(comparator) {
  
  sc_in<sc_uint<16> > comparator_in1;
  sc_in<sc_uint<16> > comparator_in2;
  sc_out<sc_uint<16> > comparator_out;
  
  

  SC_CTOR(comparator) {
	SC_THREAD(comparator_perform);     
	sensitive << comparator_in1 << comparator_in2;
     }

  
  void comparator_perform();

};

#endif
