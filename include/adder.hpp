#ifndef ADDER_HPP
#define ADDER_HPP

SC_MODULE(adder) {
  
  sc_in<sc_uint<16> > adder_in1;
  sc_in<sc_uint<16> > adder_in2;
  sc_out<sc_uint<16> > adder_out;
  
  

  SC_CTOR(adder) {
	SC_THREAD(sum_perform);     
	sensitive << adder_in1 << adder_in2;
     }

  
  void sum_perform();

};

#endif
