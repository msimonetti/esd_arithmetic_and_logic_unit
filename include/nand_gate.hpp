#ifndef nand_gate_HPP
#define nand_gate_HPP

SC_MODULE(nand_gate) {
  
  sc_in<sc_uint<16> > nand_gate_in1;
  sc_in<sc_uint<16> > nand_gate_in2;
  sc_out<sc_uint<16> > nand_gate_out;
  
  

  SC_CTOR(nand_gate) {
	SC_THREAD(nand_gate_perform);     
	sensitive << nand_gate_in1 << nand_gate_in2;
     }

  
  void nand_gate_perform();

};

#endif
