// 16bit Comparator

#ifndef Comp_HPP
#define Comp_HPP

#define NBIT 16

SC_MODULE(Comp){
	
	sc_in<sc_uint<NBIT> > Comp_input1;
	sc_in<sc_uint<NBIT> > Comp_input2;

	sc_out<sc_uint<NBIT> > Comp_output;

	SC_CTOR(Comp){
		SC_THREAD(Comp_operate);
		sensitive << Comp_input1 << Comp_input2;}

	void Comp_operate();
};

#endif
