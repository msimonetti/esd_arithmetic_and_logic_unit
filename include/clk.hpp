#ifndef CLK_HPP
#define CLK_HPP

#define SEMI 5    // semi-period

SC_MODULE(clk){
	
	sc_out<bool> clk_output;

	SC_CTOR(clk){
		SC_THREAD(clk_operate);}

	void clk_operate();
};

#endif
