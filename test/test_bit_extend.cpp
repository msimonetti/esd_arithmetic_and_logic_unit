#include <systemc.h>
#include <string>
#include "bit_extend.hpp"

using namespace std;

SC_MODULE(TestBench)
{
    sc_signal<sc_uint<16> > op;
    sc_signal<sc_uint<32> > res;
    bit_extend tb_bit_extend;
    
    SC_CTOR(TestBench) : tb_bit_extend("tb_bit_extend")
    {
        SC_THREAD(stimulus_thread);
        tb_bit_extend.bit_extend_in(this->op);
        tb_bit_extend.bit_extend_out(this->res);
        init_values();
    }

    

  private:

   void stimulus_thread() 
   {
        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            op.write(op1_values[i]);
            cout << "Operand: " << op1_values[i] << endl;
            wait(1, SC_NS);
            result_computed[i] = res.read(); 
            cout << "Operand extended: " << result_computed[i] << endl << endl;
        }
    }

    static const unsigned TEST_SIZE = 2;
    
    unsigned op1_values[TEST_SIZE];
    unsigned result_computed[TEST_SIZE];

    void init_values() 
    {
        op1_values[0] = 4;
        op1_values[1] = 65535;
        
        
	
    }


};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "..TEST IN PROGRESS.." << endl << endl;

  sc_start();

  return 0;
}
