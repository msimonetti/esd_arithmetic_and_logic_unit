#include <systemc.h>
#include <string>
#include <math.h>
#include "Comp.hpp"

using namespace std;

SC_MODULE(TB){
   public:	
	sc_signal<sc_uint<NBIT> >	input1;
	sc_signal<sc_uint<NBIT> >	input2;
	sc_signal<sc_uint<NBIT> >       equ;
	
	Comp tb_Comp;
	
	SC_CTOR(TB):tb_Comp("tb_Comp"){
		SC_THREAD(testing);
		tb_Comp.Comp_input1(this -> input1);
		tb_Comp.Comp_input2(this -> input2);
		tb_Comp.Comp_output(this -> equ);
		set_vtest();	}
   
   private:
	static const unsigned TSIZE = 3;
	unsigned vtest_in1[TSIZE],vtest_in2[TSIZE]; 
	unsigned vtest_eq[TSIZE];

	void testing(){
		for (unsigned j=0;j<TSIZE;j++)
		{
			// input test
			cout << endl << " Test " << j+1 << " of " << TSIZE<<endl;		
			input1.write(vtest_in1[j]);
			cout << "Comparator first input: " << vtest_in1[j] <<endl;
			input2.write(vtest_in2[j]);
			cout << "Comparator second input: " << vtest_in2[j] <<endl<<endl;
	
			wait(2,SC_NS);	// 2 ns
			
			vtest_eq[j] = equ.read();
			cout << "Comparator equal: "<<vtest_eq[j] <<endl;
			}
	}
	
	// set test values	
	void set_vtest(){
		vtest_in1[0] = 15;
		vtest_in2[0] = 8;
		
		vtest_in1[1] = pow(2,NBIT)-4;	// 2^NBIT-1
		vtest_in2[1] = pow(2,NBIT)-4; 

		vtest_in1[2] = 156;
		vtest_in2[2] = 2000; }
};

int sc_main(int argc, char* argv[])
{
  TB test("Comp test");

  cout << endl << "%%%%  STARTED  %%%%" << endl;

  sc_start();

  cout << endl << "%%%%  FINISHED  %%%%" << endl;  

  return 0;
}
