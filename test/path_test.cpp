#include <systemc.h>
#include <string>
#include "path.hpp"

using namespace std;

SC_MODULE(TB){
   public:	
	sc_signal<sc_uint<PATHBIT> >	input1,input2;
	sc_signal<sc_uint<PATHSEL> >	sel;
	sc_signal<sc_uint<2*PATHBIT> > 	output1;
	sc_signal<bool> op1,op2,LR;
	sc_signal<bool> load1, load2, load3;//, clk;
	
	path	tb_path;
	
	SC_CTOR(TB):tb_path("tb_path"){
		SC_THREAD(testing);
		tb_path.in_path1(this -> input1);
		tb_path.in_path2(this -> input2);
		tb_path.path_operation(this -> sel);
		tb_path.path_shift(this -> LR);
		tb_path.path_add1(this -> op1);
		tb_path.path_add2(this -> op2);
		tb_path.load_in1(this -> load1);
		tb_path.load_in2(this -> load2);
		tb_path.load_out(this -> load3);
//		tb_path.path_clk(this -> tb_clock);

		tb_path.out_path(this -> output1);
		set_vtest();	}
   
   private:
	static const unsigned TSIZE = 2;
	unsigned vtest_in1[TSIZE],vtest_in2[TSIZE],vtest_sel[TSIZE], vtest_out[TSIZE];
	bool vtest_op1[TSIZE],vtest_op2[TSIZE],vtest_LR[TSIZE];
	bool vtest_load1[TSIZE],vtest_load2[TSIZE],vtest_load3[TSIZE];

	void testing(){
		for (unsigned j=0;j<TSIZE;j++)
		{
			// input test
			cout << endl << " Test " << j+1 << " of " << TSIZE<<endl;
			input1.write(vtest_in1[j]);
			cout << "Functions (sel-input list): " << endl;
			cout << "	0: Comparator" << endl;
			cout << "	1: Multiplier" << endl;
			cout << "	2: AND" << endl;
			cout << "	3: OR" << endl;
			cout << "	4: NOT line 1" << endl;
			cout << "	5: NOT line 2" << endl;
			cout << "	6: XOR" << endl;
			cout << "	7: NAND" << endl;
			cout << "	8: NOR" << endl;
			cout << "	9: XNOR" << endl;
			cout << "	10: Adder/Incrementer" << endl;
			cout << "	11: Shifter line 1" << endl;
			cout << "	12: Shifter line 2" << endl<<endl;
			cout << "Alu input1: " << vtest_in1[j] <<endl;
			input2.write(vtest_in2[j]);
			cout << "Alu input2: " << vtest_in2[j] <<endl;
			sel.write(vtest_sel[j]);
			cout << "Alu function: " << vtest_sel[j] <<endl;
			op1.write(vtest_op1[j]);
			cout << "Adder op1: " << vtest_op1[j] <<endl;
			op2.write(vtest_op2[j]);
			cout << "Adder op2: " << vtest_op2[j] <<endl;
			LR.write(vtest_LR[j]);
			cout << "Shifter direction (0: left, 1: right): " << vtest_LR[j] <<endl<<endl;
			load1.write(vtest_load1[j]);
			cout << "Load input reg1: " << vtest_load1[j] <<endl;
			load2.write(vtest_load2[j]);
			cout << "Load input reg2: " << vtest_load2[j] <<endl;
			load3.write(vtest_load3[j]);
			cout << "Load output reg: " << vtest_load3[j] <<endl;


			wait(10,SC_NS);	// 10 ns

			vtest_out[j] = output1.read();
			cout << "Path output: "<<vtest_out[j] <<endl << "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<endl;
		}
	}
	
	// set test values	
	void set_vtest(){
		vtest_in1[0] = 1;
		vtest_in2[0] = 5;
		vtest_sel[0] = 9;
		vtest_op1[0] = 1;
		vtest_op2[0] = 1;
		vtest_load1[0] = 1;
		vtest_load2[0] = 1;
		vtest_load3[0] = 1;
		vtest_LR[0] = 1;

		vtest_in1[1] = 8;
		vtest_in2[1] = 32;
		vtest_sel[1] = 1;
		vtest_op1[1] = 1;
		vtest_op2[1] = 1;
		vtest_load1[1] = 1;
		vtest_load2[1] = 1;
		vtest_load3[1] = 1;
		vtest_LR[1] = 0; }
};

int sc_main(int argc, char* argv[])
{
  TB test("test");

  cout << endl << "%%%%  STARTED  %%%%" << endl;

  int sim_time=100;
  sc_set_time_resolution(1,SC_NS);
  sc_start(sim_time,SC_NS);
 
  cout << endl << "%%%%  FINISHED  %%%%" << endl;  

  return 0;
}
