#include <systemc.h>
#include <string>
#include "mux2.hpp"

using namespace std;

SC_MODULE(TestBench)
{
    sc_signal<sc_uint<16> > op1, op2;
    sc_signal<bool> tb_sel;
    sc_signal<sc_uint<16> > res;
    mux2 tb_mux2;
    
    SC_CTOR(TestBench) : tb_mux2("tb_mux2")
    {
        SC_THREAD(stimulus_thread);
        tb_mux2.mux2_in1(this->op1);
        tb_mux2.mux2_in2(this->op2);
        tb_mux2.mux2_sel(this->tb_sel);
        tb_mux2.mux2_out(this->res);
        init_values();
    }

    

  private:

   void stimulus_thread() 
   {
        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            op1.write(op1_values[i]);
            cout << "First Operand: " << op1_values[i] << endl;
            op2.write(op2_values[i]);
            cout << "Second Operand: " << op2_values[i] << endl;
	    tb_sel.write(tb_sel_values[i]);
            cout << "Mux Select (0 to 1): " << tb_sel_values[i] << endl;
	    wait(1, SC_NS);
            result_computed[i] = res.read(); 
            cout << "MUX out: " << result_computed[i] << endl << endl;
        }
    }

    static const unsigned TEST_SIZE = 2;
    
    unsigned op1_values[TEST_SIZE];
    unsigned op2_values[TEST_SIZE];
    unsigned tb_sel_values[TEST_SIZE];
    unsigned result_computed[TEST_SIZE];

    void init_values() 
    {
        op1_values[0] = 4;
        op1_values[1] = 27;
        
        
        op2_values[0] = 13;
        op2_values[1] = 33;


        tb_sel_values[0] = 0;
        tb_sel_values[1] = 1;
	
	
    }


};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "..TEST IN PROGRESS.." << endl << endl;

  sc_start();

  return 0;
}
