#include <systemc.h>
#include <string>
#include "alu.hpp"

using namespace std;

SC_MODULE(TestBench)
{
    sc_signal<sc_uint<16> > op1, op2;
    sc_signal<sc_uint<3> > tb_control;
    sc_signal<sc_uint<16> > res;
    alu tb_alu;
    
    SC_CTOR(TestBench) : tb_alu("tb_alu")
    {
        SC_THREAD(stimulus_thread);
        tb_alu.alu_in1(this->op1);
        tb_alu.alu_in2(this->op2);
        tb_alu.alu_op(this->tb_control);
        tb_alu.alu_out(this->res);
        init_values();
    }

    

  private:

   void stimulus_thread() 
   {
        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            op1.write(op1_values[i]);
            cout << "First Operand: " << op1_values[i] << endl;
            op2.write(op2_values[i]);
            cout << "Second Operand: " << op2_values[i] << endl;
	    tb_control.write(tb_control_values[i]);
            cout << "Operation Selected: " << tb_control_values[i] << endl;
            cout << "0: SUM" << endl;
	    cout << "1: COMPARE" << endl;
	    cout << "2: AND" << endl;
	    cout << "3: NAND" << endl;
	    cout << "4: OR" << endl;
	    cout << "5: NOR" << endl;
	    cout << "6: XOR" << endl;
	    cout << "7: XNOR" << endl;
	    	
            wait(1, SC_NS);
            result_computed[i] = res.read(); 
            cout << "ALU result: " << result_computed[i] << endl << endl;
        }
    }

    static const unsigned TEST_SIZE = 8;
    
    unsigned op1_values[TEST_SIZE];
    unsigned op2_values[TEST_SIZE];
    unsigned tb_control_values[TEST_SIZE];
    unsigned result_computed[TEST_SIZE];

    void init_values() 
    {
        op1_values[0] = 4;
        op1_values[1] = 8;
        op1_values[2] = 256;
        op1_values[3] = 8;
        op1_values[4] = 16;
        op1_values[5] = 8;
        op1_values[6] = 24;
        op1_values[7] = 2;
        
        op2_values[0] = 13;
        op2_values[1] = 2;
        op2_values[2] = 1024;
        op2_values[3] = 32;
        op2_values[4] = 2;
        op2_values[5] = 2048;
        op2_values[6] = 4096;
        op2_values[7] = 65536;
        
        tb_control_values[0] = 0;
        tb_control_values[1] = 5;
        tb_control_values[2] = 4;
        tb_control_values[3] = 7;
        tb_control_values[4] = 6;
        tb_control_values[5] = 2;
        tb_control_values[6] = 3;
        tb_control_values[7] = 1;
        
    }


};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "..TEST IN PROGRESS.." << endl << endl;

  sc_start();

  return 0;
}
