#include <systemc.h>
#include <string>
#include "alu2.hpp"

using namespace std;

SC_MODULE(TestBench)
{
    sc_signal<sc_uint<16> > op1, op2;
    sc_signal<bool> tb_add1, tb_add2, tb_shift;
    sc_signal<sc_uint<4> >  tb_operation;
    sc_signal<sc_uint<32> > res;
    alu2 tb_alu2;

    SC_CTOR(TestBench) : tb_alu2("tb_alu2")
    {
        SC_THREAD(stimulus_thread);
        tb_alu2.alu_in1(this->op1);
        tb_alu2.alu_in2(this->op2);
        tb_alu2.alu_sel_add1(this->tb_add1);
        tb_alu2.alu_sel_add2(this->tb_add2);
        tb_alu2.alu_shift(this->tb_shift);
        tb_alu2.alu_operation(this->tb_operation);
        tb_alu2.alu_out(this->res);
        init_values();
    }

    

  private:

   void stimulus_thread() 
   {
        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            op1.write(op1_values[i]);
            cout << "***********************************" << endl;
            cout << "First Operand: " << op1_values[i] << endl;
            op2.write(op2_values[i]);
            cout << "Second Operand: " << op2_values[i] << endl;
	    tb_operation.write(tb_operation_values[i]);
            cout << "***********************************" << endl;
            cout << "Operations: " << endl;
            cout << "0: COMPARE: input1 = input2 ?" << endl; 				// tested
	    cout << "1: MULTIPLICATION: input1 * input2" << endl;			// tested
	    cout << "2: AND: input1 & input2" << endl;					// tested
	    cout << "3: OR: input1 | input2" << endl;					// tested
	    cout << "4: NOT: ~input1" << endl;						// tested
	    cout << "5: NOT: ~input2" << endl;						// tested
	    cout << "6: XOR: input1 ^ input2" << endl;					// tested
	    cout << "7: NAND: ~(input1 & input2)" << endl;				// tested
	    cout << "8: NOR: ~(input1 | input2)" << endl;
	    cout << "9: XNOR: ~(input1 ^ input2)" << endl;
	    tb_add1.write(tb_add1_values[i]);
	    tb_add2.write(tb_add2_values[i]);
	    tb_shift.write(tb_shift_values[i]);
	    cout << "10: SUM: " << "ADDER select: " << tb_add1_values[i] << tb_add2_values[i]<< endl;
            cout << "----00: input1 + 1" << endl;
            cout << "----01: input1 + input2" << endl;
            cout << "----10: no operation" << endl;
            cout << "----11: input2 + 1" << endl;
	    cout << "11: SHIFT input1" << endl;
	    cout << "12: SHIFT input2:" << " SHIFTER select: " << tb_shift_values[i] << endl;
	    cout << "----0: input << 1" << endl;
            cout << "----1: input >> 1" << endl;
            cout << "13-15: return 0" << endl;
	    cout << "***********************************" << endl;
            cout << "Operation Selected: " << tb_operation_values[i] << endl;
            cout << "***********************************" << endl;
            
	    	
            wait(3, SC_NS);
            result_computed[i] = res.read(); 
            cout << "ALU result: " << result_computed[i] << endl << endl;
	    cout << "***********************************" << endl;
            cout << "***********************************" << endl;
            cout << "                                   " << endl;
            
        }
    }

    static const unsigned TEST_SIZE = 8;
    
    unsigned op1_values[TEST_SIZE];
    unsigned op2_values[TEST_SIZE];
    unsigned tb_add1_values[TEST_SIZE];
    unsigned tb_add2_values[TEST_SIZE];
    unsigned tb_shift_values[TEST_SIZE];
    unsigned tb_operation_values[TEST_SIZE];
    unsigned result_computed[TEST_SIZE];

    void init_values() 
    {
        op1_values[0] = 13;
        op1_values[1] = 8;
        op1_values[2] = 7245;
        op1_values[3] = 1;
        op1_values[4] = 4294967294;
        op1_values[5] = 1;
        op1_values[6] = 24;
        op1_values[7] = 2;
        
        op2_values[0] = 14;
        op2_values[1] = 2;
        op2_values[2] = 13356;
        op2_values[3] = 0;
        op2_values[4] = 1;
        op2_values[5] = 4294967294;
        op2_values[6] = 4096;
        op2_values[7] = 25;
        

	tb_add1_values[0] = 0;
	tb_add1_values[1] = 1;
	tb_add1_values[2] = 1;
	tb_add1_values[3] = 0;
	tb_add1_values[4] = 1;
	tb_add1_values[5] = 0;
	tb_add1_values[6] = 0;	
	tb_add1_values[7] = 0;

	tb_add2_values[0] = 1;
	tb_add2_values[1] = 1;
	tb_add2_values[2] = 0;
	tb_add2_values[3] = 0;
	tb_add2_values[4] = 1;
	tb_add2_values[5] = 0;
	tb_add2_values[6] = 0;	
	tb_add2_values[7] = 1;

	tb_shift_values[0] = 0;
	tb_shift_values[1] = 1;
	tb_shift_values[2] = 0;
	tb_shift_values[3] = 0;
	tb_shift_values[4] = 1;
	tb_shift_values[5] = 0;
	tb_shift_values[6] = 0;	
	tb_shift_values[7] = 1;


        tb_operation_values[0] = 0;  // compare
        tb_operation_values[1] = 1;  // mult
        tb_operation_values[2] = 2;  // and
        tb_operation_values[3] = 3;  // or
        tb_operation_values[4] = 4;  // not1
        tb_operation_values[5] = 5;  // not2
        tb_operation_values[6] = 6;  // xor
        tb_operation_values[7] = 7;  // nand
        
    }


};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "..TEST IN PROGRESS.." << endl << endl;

  sc_start();

  return 0;
}
