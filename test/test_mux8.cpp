#include <systemc.h>
#include <string>
#include "mux8.hpp"

using namespace std;

SC_MODULE(TestBench)
{
    sc_signal<sc_uint<16> > op1, op2, op3, op4, op5, op6, op7, op8;
    sc_signal<sc_uint<3> > tb_sel;
    sc_signal<sc_uint<16> > res;
    mux8 tb_mux8;
    
    SC_CTOR(TestBench) : tb_mux8("tb_mux8")
    {
        SC_THREAD(stimulus_thread);
        tb_mux8.mux8_in1(this->op1);
        tb_mux8.mux8_in2(this->op2);
        tb_mux8.mux8_in3(this->op3);
        tb_mux8.mux8_in4(this->op4);
        tb_mux8.mux8_in5(this->op5);
        tb_mux8.mux8_in6(this->op6);
        tb_mux8.mux8_in7(this->op7);
        tb_mux8.mux8_in8(this->op8);
        tb_mux8.mux8_sel(this->tb_sel);
        tb_mux8.mux8_out(this->res);
        init_values();
    }

    

  private:

   void stimulus_thread() 
   {
        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            op1.write(op1_values[i]);
            cout << "First Operand: " << op1_values[i] << endl;
            op2.write(op2_values[i]);
            cout << "Second Operand: " << op2_values[i] << endl;
	    op3.write(op3_values[i]);
            cout << "Third Operand: " << op3_values[i] << endl;
            op4.write(op4_values[i]);
            cout << "Fourth Operand: " << op4_values[i] << endl;
	    op5.write(op5_values[i]);
            cout << "Fifth Operand: " << op5_values[i] << endl;
            op6.write(op6_values[i]);
            cout << "Sixth Operand: " << op6_values[i] << endl;
	    op7.write(op7_values[i]);
            cout << "Seventh Operand: " << op7_values[i] << endl;
            op8.write(op8_values[i]);
            cout << "Eighth Operand: " << op8_values[i] << endl;
	    tb_sel.write(tb_sel_values[i]);
            cout << "Mux Select (0 to 7): " << tb_sel_values[i] << endl;
	    wait(1, SC_NS);
            result_computed[i] = res.read(); 
            cout << "MUX out: " << result_computed[i] << endl << endl;
        }
    }

    static const unsigned TEST_SIZE = 2;
    
    unsigned op1_values[TEST_SIZE];
    unsigned op2_values[TEST_SIZE];
    unsigned op3_values[TEST_SIZE];
    unsigned op4_values[TEST_SIZE];
    unsigned op5_values[TEST_SIZE];
    unsigned op6_values[TEST_SIZE];
    unsigned op7_values[TEST_SIZE];
    unsigned op8_values[TEST_SIZE];
    unsigned tb_sel_values[TEST_SIZE];
    unsigned result_computed[TEST_SIZE];

    void init_values() 
    {
        op1_values[0] = 4;
        op1_values[1] = 27;
        
        
        op2_values[0] = 13;
        op2_values[1] = 33;


        op3_values[0] = 1;
        op3_values[1] = 16;
        
        
        op4_values[0] = 256;
        op4_values[1] = 1024;

	
        op5_values[0] = 417;
        op5_values[1] = 326;
        
        
        op6_values[0] = 134;
        op6_values[1] = 65;

	
        op7_values[0] = 97;
        op7_values[1] = 128;
        
        
        op8_values[0] = 2;
        op8_values[1] = 8;
	

	tb_sel_values[0] = 2;
        tb_sel_values[1] = 7;
	
	
    }


};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "..TEST IN PROGRESS.." << endl << endl;

  sc_start();

  return 0;
}
