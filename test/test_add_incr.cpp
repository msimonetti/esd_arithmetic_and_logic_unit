#include <systemc.h>
#include <string>
#include "add_incr.hpp"

using namespace std;

SC_MODULE(TestBench)
{
    sc_signal<sc_uint<16> > op1, op2;
    sc_signal<bool> tb_control1, tb_control2;
    sc_signal<sc_uint<16> > res;
    add_incr tb_add_incr;
    
    SC_CTOR(TestBench) : tb_add_incr("tb_add_incr")
    {
        SC_THREAD(stimulus_thread);
        tb_add_incr.add_incr_in1(this->op1);
        tb_add_incr.add_incr_in2(this->op2);
        tb_add_incr.add_incr_op1(this->tb_control1);
        tb_add_incr.add_incr_op2(this->tb_control2);
        tb_add_incr.add_incr_out(this->res);
        init_values();
    }

    

  private:

   void stimulus_thread() 
   {
        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            op1.write(op1_values[i]);
            cout << "First Operand: " << op1_values[i] << endl;
            op2.write(op2_values[i]);
            cout << "Second Operand: " << op2_values[i] << endl;
	    tb_control1.write(tb_control1_values[i]);
            cout << "MUX1 control (0 to 1): " << tb_control1_values[i] << endl;
            cout << "0: A" << endl;
	    cout << "1: INCREMENT" << endl;
	    
	    tb_control2.write(tb_control2_values[i]);
            cout << "MUX2 control (0 to 1): " << tb_control2_values[i] << endl;
            cout << "0: INCREMENT" << endl;
	    cout << "1: B" << endl;
	    	
            wait(1, SC_NS);
            result_computed[i] = res.read(); 
            cout << "Result Computed: " << result_computed[i] << endl << endl;
        }
    }

    static const unsigned TEST_SIZE = 8;
    
    unsigned op1_values[TEST_SIZE];
    unsigned op2_values[TEST_SIZE];
    unsigned tb_control1_values[TEST_SIZE];
    unsigned tb_control2_values[TEST_SIZE];
    unsigned result_computed[TEST_SIZE];

    void init_values() 
    {
        op1_values[0] = 4;
        op1_values[1] = 8;
        op1_values[2] = 256;
        op1_values[3] = 8;
        op1_values[4] = 16;
        op1_values[5] = 8;
        op1_values[6] = 24;
        op1_values[7] = 2;
        
        op2_values[0] = 13;
        op2_values[1] = 2;
        op2_values[2] = 1024;
        op2_values[3] = 32;
        op2_values[4] = 2;
        op2_values[5] = 2048;
        op2_values[6] = 4096;
        op2_values[7] = 65536;
        
        tb_control1_values[0] = 0;
        tb_control1_values[1] = 1;
        tb_control1_values[2] = 0;
        tb_control1_values[3] = 0;
        tb_control1_values[4] = 1;
        tb_control1_values[5] = 1;
        tb_control1_values[6] = 0;
        tb_control1_values[7] = 1;
        
	tb_control2_values[0] = 1;
        tb_control2_values[1] = 0;
        tb_control2_values[2] = 1;
        tb_control2_values[3] = 1;
        tb_control2_values[4] = 0;
        tb_control2_values[5] = 0;
        tb_control2_values[6] = 0;
        tb_control2_values[7] = 1;
        
    }


};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "..TEST IN PROGRESS.." << endl << endl;

  sc_start();

  return 0;
}
